// add command
const { authLogin } = require('../middleware/auth');
mp.events.addCommand('hp', (player) => {
    player.health = 100;
    player.armour = 100;
});

mp.events.addCommand('testToken', (player) => {
    let checkLogin = authLogin(player);
    if(checkLogin.error) return;
    console.log(player.user);
});

mp.events.addCommand('testToken2', player => {
    player.token = null;
})
mp.events.addCommand('getpos', (player, fullText) => {
    if (fullText == undefined) {
        player.outputChatBox("/getpos [day la dau ?]");
        return;
    }
    player.outputChatBox(`${player.position.x} ${player.position.y} ${player.position.z} ${fullText}`);
})
mp.events.addCommand("tp", (player, fullText, x, y, z) => {
    if (x == undefined || y == undefined || z == undefined) {
        player.outputChatBox('/tp [x] [y] [z]');
        return;
    }
    player.outputChatBox("Ban da teleport!!!");
    player.position = new mp.Vector3(parseFloat(x), parseFloat(y), parseFloat(z));
});
mp.events.addCommand('vehicle', (player, fullText, vehicle, numberPlate) => {
    if (vehicle == undefined || numberPlate == undefined) {
        player.outputChatBox('/vehicle [vehicle] [numberPlate]');
        return;
    }
    var check_spawnvehicle = player.getVariable('veh');
    if (check_spawnvehicle != null) {
        check_spawnvehicle.destroy();
    }
    let spawncar = mp.vehicles.new(mp.joaat(vehicle), new mp.Vector3(player.position.x - 4, player.position.y - 4, player.position.z),
        { engine: true, locked: false, color: [[0, 255, 0], [0, 255, 0]] });
    player.setVariable('veh', spawncar);

})
mp.events.addCommand('veh', (player, fullText, vehicle, bangso) => {
    if (vehicle == undefined) {
        player.outputChatBox('/vehicle [vehicle]');
        return;
    }

    mp.vehicles.new(mp.joaat(vehicle), new mp.Vector3(player.position.x, player.position.y, player.position.z),
        { engine: true, locked: false, color: [[255, 255, 255], [255, 255, 255]], numberPlate: "123" });
})
mp.events.addCommand('refresh', (player) => {
    player.removeAllWeapons();
    player.setClothes(0, 0, 0, 2);
    player.setClothes(1, 0, 0, 2);
    player.setClothes(2, 0, 0, 2);
    player.setClothes(3, 0, 0, 2);
    player.setClothes(4, 0, 0, 2);
    player.setClothes(5, 0, 0, 2);
    player.setClothes(6, 0, 0, 2);
    player.setClothes(7, 0, 0, 2);
    player.setClothes(8, 0, 0, 2);
    player.setClothes(9, 0, 0, 2);
    player.setClothes(10, 0, 0, 2);
    player.setClothes(11, 0, 0, 2);
})
mp.events.addCommand("checkVehicle", (player) => {
})

mp.events.addCommand('health', (player)=>{
    player.health = 100;
})

mp.events.addCommand("getPos", (player, fullText) => {
    player.outputChatBox("x: " + player.position.x + "y: " + player.position.y + "z: " + player.position.z);
});

mp.events.addCommand("spawn",(player) =>{
    if(player.isDead)
        player.spawn(new mp.Vector3(player.position.x, player.position.y, player.position.z));
    else player.outputChatBox("Can't spawn");
})
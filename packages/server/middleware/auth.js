const jwt = require('jsonwebtoken');
const { privateKey } = require('../const/const')

module.exports.authLogin = (player) =>{
    if(player.token){
        try {
            let user = jwt.verify(player.token, privateKey)
            player.user = user.userExist;
            return {error: false}
        } catch (error) {
            player.token = null
            //hien thi khung login
            player.call("switchToLoginForm");
            return {error: true}
        }
    } else {
        //Hien thi khung Login
        player.call("switchToLoginForm");
        return {error: true}
    }
}
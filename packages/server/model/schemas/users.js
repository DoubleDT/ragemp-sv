'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    jobId:{
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    money: {
      type: DataTypes.INTEGER,
      defaultValue: 500
    },
    bank: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    blood: {
      type: DataTypes.INTEGER,
      defaultValue: 100
    },
    armor: {
      type: DataTypes.INTEGER,
      defaultValue: 100
    },
    sex: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    posX:{
      type: DataTypes.INTEGER,
      defaultValue: 15
    },
    posY:{
      type: DataTypes.INTEGER,
      defaultValue: 15
    },
    posZ:{
      type: DataTypes.INTEGER,
      defaultValue: 15
    },
    adminLevel: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    // User.hasMany(models["items"], { foreignKey: 'owner'})
    // User.hasMany(models["vehicles"], { foreignKey: 'owner'})

  };
  return User;
};

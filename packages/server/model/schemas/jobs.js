'use strict';

module.exports = (sequelize, DataTypes) => {
  const Jobs = sequelize.define('jobs', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  Jobs.associate = function(models) {
    // associations can be defined here
    // Jobs.belongsTo(models['users'])
  };
  return Jobs;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Clothes = sequelize.define('clothes', {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    head: {
      type: DataTypes.STRING,
      allowNull: true
    },
    beard: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hair: {
      type: DataTypes.STRING,
      allowNull: true
    },
    torso: {
      type: DataTypes.STRING,
      allowNull: true
    },
    legs: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hands: {
      type: DataTypes.STRING,
      allowNull: true
    },
    foot: {
      type: DataTypes.STRING,
      allowNull: true
    },
    eyes: {
      type: DataTypes.STRING,
      allowNull: true
    },
    parachute: {
      type: DataTypes.STRING,
      allowNull: true
    },
    bags: {
      type: DataTypes.STRING,
      allowNull: true
    },
    mask: {
      type: DataTypes.STRING,
      allowNull: true
    },
    auxiliary: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {});
  Clothes.associate = function(models) {
    // associations can be defined here
    Clothes.belongsTo(models['users'])
  };
  return Clothes;
};
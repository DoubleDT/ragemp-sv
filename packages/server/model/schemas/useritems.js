'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserItems = sequelize.define('useritems', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    itemsId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    qty: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
  }, {});
  UserItems.associate = function(models) {
    // associations can be defined here
    UserItems.belongsTo(models['users'])
    UserItems.belongsTo(models['items'])
  };
  return UserItems;
};
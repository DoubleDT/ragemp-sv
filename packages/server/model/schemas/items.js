module.exports = (sequelize, DataTypes) => {
  const Items = sequelize.define('items', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    price: {
      defaultValue: false,
      type: DataTypes.DOUBLE,
      allowNull: true
    },
  }, {});
  Items.associate = function (models) {
    // associations can be defined here
  };
  return Items;
};

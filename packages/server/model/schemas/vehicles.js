'use strict';

module.exports = (sequelize, DataTypes) => {
  const Vehicles = sequelize.define('vehicles', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    type: {
      type: DataTypes.INTEGER,
    },
    model: {
      type: DataTypes.STRING
    },
    owner: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    isLocked: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    door: {
      type: DataTypes.INTEGER,
      defaultValue: 100
    },
    engine: {
      type: DataTypes.INTEGER,
      defaultValue: 100
    },
    wheel: {
      type: DataTypes.INTEGER,
      defaultValue: 100
    },
    gas: {
      type: DataTypes.DECIMAL,
      defaultValue: 100
    },
    kms: {
      type: DataTypes.DECIMAL,
      defaultValue: 0
    },
    price: {
      type: DataTypes.INTEGER,
    },
    posX: {
      type: DataTypes.INTEGER,
      defaultValue: 15
    },
    posY: {
      type: DataTypes.INTEGER,
      defaultValue: 15
    },
    posZ: {
      type: DataTypes.INTEGER,
      defaultValue: 15
    },
  }, {});
  Vehicles.associate = function (models) {
    // associations can be defined here

  };
  return Vehicles;
};

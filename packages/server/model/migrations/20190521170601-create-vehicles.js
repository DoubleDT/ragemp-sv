'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vehicles', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      type: {
        type: Sequelize.INTEGER,
      },
      model: {
        type: Sequelize.STRING
      },
      owner: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      isLocked: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      door: {
        type: Sequelize.INTEGER,
        defaultValue: 100
      },
      engine: {
        type: Sequelize.INTEGER,
        defaultValue: 100
      },
      wheel: {
        type: Sequelize.INTEGER,
        defaultValue: 100
      },
      gas: {
        type: Sequelize.DECIMAL,
        defaultValue: 100
      },
      kms: {
        type: Sequelize.DECIMAL,
        defaultValue: 0
      },
      price: {
        type: Sequelize.INTEGER,
      },
      posX: {
        type: Sequelize.INTEGER,
        defaultValue: 15
      },
      posY: {
        type: Sequelize.INTEGER,
        defaultValue: 15
      },
      posZ: {
        type: Sequelize.INTEGER,
        defaultValue: 15
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vehicles');
  }
};
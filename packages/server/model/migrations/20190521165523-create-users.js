'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      jobId:{
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      username: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING
      },
      money: {
        type: Sequelize.INTEGER,
        defaultValue: 500
      },
      bank: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      blood: {
        type: Sequelize.INTEGER,
        defaultValue: 100
      },
      armor: {
        type: Sequelize.INTEGER,
        defaultValue: 100
      },
      sex: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      posX:{
        type: Sequelize.INTEGER,
        defaultValue: 15
      },
      posY:{
        type: Sequelize.INTEGER,
        defaultValue: 15
      },
      posZ:{
        type: Sequelize.INTEGER,
        defaultValue: 15
      },
      adminLevel: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};
'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('clothes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.STRING,
        allowNull: false
      },
      head: {
        type: Sequelize.STRING,
        allowNull: true
      },
      beard: {
        type: Sequelize.STRING,
        allowNull: true
      },
      hair: {
        type: Sequelize.STRING,
        allowNull: true
      },
      torso: {
        type: Sequelize.STRING,
        allowNull: true
      },
      legs: {
        type: Sequelize.STRING,
        allowNull: true
      },
      hands: {
        type: Sequelize.STRING,
        allowNull: true
      },
      foot: {
        type: Sequelize.STRING,
        allowNull: true
      },
      eyes: {
        type: Sequelize.STRING,
        allowNull: true
      },
      parachute: {
        type: Sequelize.STRING,
        allowNull: true
      },
      bags: {
        type: Sequelize.STRING,
        allowNull: true
      },
      mask: {
        type: Sequelize.STRING,
        allowNull: true
      },
      auxiliary: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('clothes');
  }
};
const {jobType} = require('../const/const')

class PizzaBoyService{
    static async getPizza(player){
        if(player.pizza > 0)
            return player.outputChatBox("Ban ko the lay them pizza")
        player.outputChatBox("Ban da lay thanh cong 1 pizza")
        player.outputChatBox("Ban can giao hang den 923.5 3143.5")
        player.pizza = 1;
        mp.events.call("generatePizzaDes",player)
        return player.call("deliverPizza")
    }

    static async onDuty(player){
        player.outputChatBox("da thay dong phuc pizzaboy")
        player.setClothes(3, 25, 0, 2);
        player.setClothes(4, 52, 0, 2);
        player.setClothes(6, 25, 0, 2);
        player.setClothes(7, 52, 0, 2);
        player.setClothes(8, 25, 0, 2);
    }

    static async offDuty(player){
        player.outputChatBox("da thay dong phuc thuong`")
        player.setClothes(3, 69, 0, 2);
        player.setClothes(4, 96, 0, 2);
        player.setClothes(6, 69, 0, 2);
        player.setClothes(7, 96, 0, 2);
        player.setClothes(8, 10, 0, 2);
    }
}

module.exports = PizzaBoyService
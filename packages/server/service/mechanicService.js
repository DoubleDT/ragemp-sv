const {colShapes, jobType} = require('../const/const')

class mechanicService  {
    //load colshape and marker G Store
    static async loadColshape(mechanicBlip){
        return mechanicBlip.map(a=>{
            let mechanicColshape = mp.colshapes.newSphere(a.x,a.y,a.z,1);
            mechanicColshape.setVariable('SetC',colShapes.mechanic.npc)

            mp.markers.new(1, new mp.Vector3(a.x, a.y, a.z-1), {drawDistance:10});

            mp.labels.new("Tho sua xe truong", new mp.Vector3(a.x, a.y, a.z+1), {
                color: [255, 255, 255, 100]
            });
        })
    }
    //open Menu
    static async openMechanicNPC(player){
        player.call('openMechanicNPCClient');
    }

    static async openMechanicMenu(player){
        player.call('openMechanicMenuClient')
    }

    static async bindKeyOpenMechanicNPC(player){
        player.call('bindKeyToOpenMechanicNPC');
    }

    static async bindKeyOpenMechanicMenu(player){
        player.call('bindKeyToOpenMechanicMenu');
    }

    static async setMechanic(player){
        player.job = jobType.mechanic;
        player.outputChatBox("mechanic")
        player.call("bindKeyToOpenMechanicMenu");
    }

    static async repairVeh(player){
        player.outputChatBox("sua xe");
    }

    static async cleanVeh(player){
        player.outputChatBox("rua xe");
    }

    static async destroyColshape(player){
        mp.colshapes.at(0).destroy();
    }
};

module.exports = mechanicService;
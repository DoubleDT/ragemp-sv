const {colShapes} = require('../const/const')

class wantedService  {
    //load colshape and marker Wanted
    static async loadColshape(wantedBlip){
        return wantedBlip.map(a=>{
            let wantedColshape = mp.colshapes.newSphere(a.x,a.y,a.z,1);
            wantedColshape.setVariable('SetC',colShapes.wanted)

            mp.markers.new(1, new mp.Vector3(a.x, a.y, a.z-1), {drawDistance:10});

            mp.labels.new("Phat lenh truy na", new mp.Vector3(a.x, a.y, a.z+1), {
                color: [255, 255, 255, 100]
            });
        })
    }
    //open Menu
    static async openWantedMenu(player){
        player.call('openWantedMenuClient');
    }

    static async bindKey(player){
        player.call('bindKeyToOpenWantedMenu');
    }
    //player purchased G Store
    static async emitWanted(player, wantedID){
        player.outputChatBox('Truy na: ' + wantedID);
    }

    static async destroyColshape(player){
        mp.colshapes.at(0).destroy();
    }
};

module.exports = wantedService;
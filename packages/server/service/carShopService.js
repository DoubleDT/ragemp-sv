const {colShapes} = require('../const/const')

class carShopService  {
    //load colshape and marker car shop
    static async loadColshape(carShopBlip){
        return carShopBlip.map(a=>{
            let carShopColshape = mp.colshapes.newSphere(a.x,a.y,a.z,1);
            carShopColshape.setVariable('SetC',colShapes.carShop)

            mp.markers.new(1, new mp.Vector3(a.x, a.y, a.z-1), {drawDistance:10});

            mp.labels.new("Chu cua hang xe", new mp.Vector3(a.x, a.y, a.z+1), {
                color: [255, 255, 255, 100]
            });
        })
    }
    //open Menu
    static async openCarShop(player){
        return player.call('openCarShopClient');
    }

    static async bindKey(player){
        return player.call('bindKeyToOpenCarShop');
    }
    //player purchased Car
    static async purchaseCar(player, car, carPrice){
        if(player.money < carPrice){
            player.outputChatBox("Ban ko du tien mua xe")
            return;
        }
        
        player.ownCar += 1;
        player.car.push(car);
        player.money -= carPrice;
        player.outputChatBox("Ban da mua thanh cong 1 chiec oto " + car +" voi gia la " + carPrice)
        mp.vehicles.new(mp.joaat(car), new mp.Vector3(player.position.x+0.5,player.position.y+0.5,player.position.z+2),
        {
            numberPlate: "ADMIN",
            color: [[0, 255, 0],[0, 255, 0]]
        });
    }

    static async destroyColshape(player){
        mp.colshapes.at(0).destroy();
    }
};

module.exports = carShopService;
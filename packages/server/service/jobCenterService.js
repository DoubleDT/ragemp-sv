const {colShapes, jobType} = require('../const/const')

class jobCenterService  {
    //load colshape and marker job center
    static async loadColshape(jobCenterBlip){
        return jobCenterBlip.map(a=>{
            let jobCenterBlip = mp.colshapes.newSphere(a.x,a.y,a.z,1);
            jobCenterBlip.setVariable('SetC',colShapes.jobCenter)

            mp.markers.new(1, new mp.Vector3(a.x, a.y, a.z-1), {drawDistance:10});

            mp.labels.new("Trung tam viec lam", new mp.Vector3(a.x, a.y, a.z+1), {
                color: [255, 255, 255, 100]
            });
        })
    }
    //open Menu
    static async openJobCenter(player){
        return player.call('openJobCenterClient');
    }

    static async bindKey(player){
        return player.call('bindKeyToOpenJobCenter');
    }

    static async setJob(player,job){
        player.job = job;
        player.outputChatBox('abc');
    }

    static async destroyColshape(player){
        mp.colshapes.at(0).destroy();
    }
};

module.exports = jobCenterService;
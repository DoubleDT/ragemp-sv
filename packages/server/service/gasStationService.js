const {colShapes} = require('../const/const')

class gasStationService  {
    //load colshape and marker car shop
    // dung: can tét lai
    static async loadColshape(gasStationBlip){
        return gasStationBlip.map(a=>{
            let gasStationColshape = mp.colshapes.newSphere(a.x,a.y,a.z,1);
            gasStationColshape.setVariable('SetC',colShapes.gasStation)

            mp.markers.new(1, new mp.Vector3(a.x, a.y, a.z-1), {drawDistance:10});

            mp.labels.new("Nhan vien cay xang", new mp.Vector3(a.x, a.y, a.z+1), {
                color: [255, 255, 255, 100]
            });
        })
        // let gasStationMarker = gasStationBlip.map(b=>{
        //     mp.markers.new(1, new mp.Vector3(b.x, b.y, b.z-1), {drawDistance:10});
        // })
        // let gasStationLabel = gasStationBlip.map(c=>{
        //     mp.labels.new("Nhan vien cay xang", new mp.Vector3(c.x, c.y, c.z+1), {
        //         color: [255, 255, 255, 100]
        //     });
        // })
    }
    //open Menu
    static async openGasStation(player){
        player.call('openGasStationClient');
    }

    static async bindKey(player){
        player.call('bindKeyToOpenGasStation');
    }
    //fill up fuel tank
    static async fillUp(player, car){
        
    }

    static async destroyColshape(player){
        mp.colshapes.at(0).destroy();
    }
};

module.exports = gasStationService;
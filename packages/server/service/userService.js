const users = require('../model/schemas/index').users;
const items = require('../model/schemas/index').items;
const { privateKey } = require('../const/const')

const jwt = require('jsonwebtoken');

const {
    itemType,
    vehiclesType,
    adminLevel,
    jobType
} = require('../const/const')
const md5 = require('md5')

class UserService {
    static async createUser({ username, password }) {
        return new Promise(async resolve => {
            let checkUserExist = await users.findOne({ where: { username } });
            if (checkUserExist)
                return resolve({ error: true, message: "User da ton tai" });

            let newUser = {
                username: username,
                password: md5(password),
                money: 100000,
                bank: 100000,
                blood: 100,
                armor: 100,
                sex: 1,
                posX: 191,
                posY: 191,
                posZ: 191,
                job: 0,
                adminLevel: adminLevel.user
            };
            await users.create(newUser);
            return resolve({ error: false, message: "" })
        });
    }

    static async login({username, password}) {
        let password0 = md5(password);
        return new Promise(async resolve => {
            let userExist = await users.findOne({ where: { username } });
            if(userExist){
                let loginSuccess = await userExist.dataValues.password == password0;
                if (loginSuccess){
                    delete userExist.password;
                    let token = jwt.sign({userExist}, privateKey)
                    return resolve({ error: false, message: "Dang nhap thanh cong", token });
                } else return resolve({error: true, message: "Sai mat khau" });
            } else return resolve({ error: true, message: "Khong ton tai username" });
        })
    };

    static async saveInfo(player) {
        return new Promise(resolve => {
            users.update({
                money: player.data.money,
                posX: player.position.x.toFixed(2),
                posY: player.position.y.toFixed(2),
                posZ: player.position.z.toFixed(2),
                blood: player.health,
                armor: player.armour,
            });
        })
    };
}


module.exports = UserService;
const {jobType} = require('../const/const')

class ColShapeService {
    static async giveWeapon(player) {
        player.removeWeapon();
        player.giveWeapon(0xFAD1F1C9, 100);
        player.giveWeapon(0xBFE256D4, 50);
        player.giveWeapon(0x678B81B1, 100);
        player.giveWeapon(0x3656C8C1, 100);
        player.setClothes(3, 0, 0, 2);
        player.setClothes(8, 58, 0, 2);
        player.setClothes(6, 25, 0, 2);
        player.setClothes(4, 35, 0, 2);
        player.setClothes(11, 55, 0, 2);
    }
    
    static async setTrucker(player) {
        player.setClothes(3, 73, 0, 2);
        player.setClothes(4, 48, 0, 2);
        player.setClothes(6, 27, 0, 2);
        player.setClothes(7, 36, 0, 2);
        player.setClothes(8, 96, 0, 2);
        player.setClothes(11, 9, 0, 2);
    }
    static async Offdutytrucker(player) {
        player.setClothes(3, 0, 0, 2);
        player.setClothes(4, 0, 0, 2);
        player.setClothes(6, 0, 0, 2);
        player.setClothes(7, 0, 0, 2);
        player.setClothes(8, 0, 0, 2);
        player.setClothes(11, 0, 0, 2);
    }
    static async openMenu(player){
        let res = false;
        if(player.job == jobType.shipper)
            res = true
        return player.call("enterPizzaColshapeClient", [res]);
    }

    static async spawnPizzaVehicle(player){
        return player.call("bindKeyToSpawnVeh")
    }

    static async deliverPizza(player){
        if(player.pizza > 0){
            player.money += 100;
            player.outputChatBox("Ban da giao hang thanh cong. Tien thuong la 100$. Tien hien tai cua ban la " + player.money +"$")
            player.pizza= 0;
        }
    }

    static async carShop(player){
        mp.events.call('playerEnterColdShape-carShop',player)
    }

    static async gasStation(player){
        mp.events.call('playerEnterColdShape-gasStation',player)
    }

    static async gStore(player){
        mp.events.call('playerEnterColdShape-gStore',player);
    }

    static async mechanic(player){
        mp.events.call('playerEnterColdShape-mechanicNPC',player);
    }

    static async jobCenter(player){
        mp.events.call('playerEnterColdShape-jobCenter', player);
    }

    static async wanted(player){
        mp.events.call('playerEnterColdShape-wanted', player);
    }
}


module.exports = ColShapeService;
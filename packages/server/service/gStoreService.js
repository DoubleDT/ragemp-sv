const {colShapes} = require('../const/const')

class gStoreService  {
    //load colshape and marker G Store
    static async loadColshape(gStoreBlip){
        return gStoreBlip.map(a=>{
            let gStoreColshape = mp.colshapes.newSphere(a.x,a.y,a.z,1);
            gStoreColshape.setVariable('SetC',colShapes.gStore)

            mp.markers.new(1, new mp.Vector3(a.x, a.y, a.z-1), {drawDistance:10});

            mp.labels.new("Chu cua hang tien loi 24/7", new mp.Vector3(a.x, a.y, a.z+1), {
                color: [255, 255, 255, 100]
            });
        })
    }
    //open Menu
    static async openGStore(player){
        player.call('openGStoreClient');
    }

    static async bindKey(player){
        player.call('bindKeyToOpenGStore');
    }
    //player purchased G Store
    static async purchaseItem(player, item, price){
        if(player.money < price){
            player.outputChatBox("Ban ko du tien mua item")
            return;
        }
        
        // player.item += 1;
        player.money -= price;
        player.outputChatBox("Ban da mua thanh cong " + item +" voi gia la " + price)
    }

    static async destroyColshape(player){
        mp.colshapes.at(0).destroy();
    }
};

module.exports = gStoreService;
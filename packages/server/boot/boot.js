const md5 = require('md5');
const db = require('../model/schemas/index')
const { itemType, vehiclesType, adminLevel, carShopBlip, gasStationBlip, gStoreBlip, mechanicBlip, jobCenterBlip, wantedBlip } = require('../const/const')
const carShopService = require('../service/carShopService')
const gasStationService = require('../service/gasStationService')
const gStoreService = require('../service/gStoreService')
const mechanicService = require('../service/mechanicService')
const jobCenterService = require('../service/jobCenterService')
const wantedService = require('../service/wantedService')

async function bootAll() {
    const superAdmin = {
        username: "admin",
        jobId: 0,
        password: md5("phamthanhthao220518"),
        money: 100000,
        bank: 100000,
        blood: 100,
        armor: 100,
        sex: 1,
        posX: 191,
        posY: 191,
        posZ: 191,
        adminLevel: adminLevel.superAdmin
    }

    const jobs = [
        { name: 'Trucker'},
        { name: 'Police'},
        { name: 'emergency'},
        { name: 'shipper'},
        { name: 'mechanic'},
    ]
    const items = [
        { name: 'Water', price: 100 },
        { name: 'Bread', price: 100 },
        { name: 'Pet Food', price: 100 },
        { name: 'Bullet Mag', price: 100 },
        { name: 'Bandage', price: 100 },
        { name: 'First Aid Kit', price: 100 },
        { name: 'Knife', price: 100 },
        { name: 'Baton', price: 100 },
        { name: 'Baseball Bat', price: 100 },
        { name: 'Flashlight', price: 100 },
        { name: 'Electric Gun', price: 100 },
        { name: 'Piston', price: 100 },
        { name: 'Uzi', price: 100 },
        { name: 'Shortgun', price: 100 },
        { name: 'SMG', price: 100 },
        { name: 'Sniper Rifle', price: 100 },
        { name: 'Heavy Sniper', price: 100 },
        { name: 'AKM', price: 100 },
        { name: 'Minigun', price: 100 },
        { name: 'Gas Can', price: 100 },
    ]
    // init super admin
    db.users.findOne({ where: { username: "admin" } })
        .then(admin => {
            if (!admin) {
                db.users.create(superAdmin).then(() => {
                    console.log("Super admin was created")
                })
            }
        })
    console.log('Boot init data...');

    await Promise.all([
        // db.users.upsert({ username: 'admin'}, superAdmin),
        ...jobs.map(job => {
            db.jobs.findOne({ where: { name: job.name } })
                .then(admin => {
                    if (!admin) {
                        db.jobs.create(job).then(() => {
                            console.log("Jobs was created")
                        })
                    }
                })
        }),
        ...items.map(item => {
            db.items.findOne({ where: { name: item.name } })
                .then(admin => {
                    if (!admin) {
                        db.items.create(item).then(() => {
                            console.log("Items was created")
                        })
                    }
                })
        }),
        carShopService.loadColshape(carShopBlip),
        gasStationService.loadColshape(gasStationBlip),
        gStoreService.loadColshape(gStoreBlip),
        mechanicService.loadColshape(mechanicBlip),
        jobCenterService.loadColshape(jobCenterBlip),
        wantedService.loadColshape(wantedBlip),
    ])
    console.log('Booted!')
    // db.jobs.bulkCreate(jobs).then(() => {
    //     console.log("Jobs was created");
    // })

    // db.items.bulkCreate(items).then(() => {
    //     console.log("Items was created");
    // })

    carShopService.loadColshape(carShopBlip);
    gasStationService.loadColshape(gasStationBlip);
    gStoreService.loadColshape(gStoreBlip);
    mechanicService.loadColshape(mechanicBlip);
    jobCenterService.loadColshape(jobCenterBlip);
    wantedService.loadColshape(wantedBlip);
}

bootAll()

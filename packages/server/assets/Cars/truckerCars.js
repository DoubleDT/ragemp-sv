
const car = [
    { id: 0, cartype: "mule", numberPlate: "Truck 1", locked: false, position: new mp.Vector3(893, -3129, 6), heading: 0},
    { id: 1, cartype: "mule", numberPlate: "Truck 2", locked: false, position: new mp.Vector3(897, -3129, 6), heading: 0},
    { id: 2, cartype: "mule", numberPlate: "Truck 3", locked: false, position: new mp.Vector3(901, -3129, 6), heading: 0},
    { id: 3, cartype: "mule", numberPlate: "Truck 4", locked: false, position: new mp.Vector3(905, -3129, 6), heading: 0},
    { id: 4, cartype: "mule", numberPlate: "Truck 5", locked: false, position: new mp.Vector3(909, -3129, 6), heading: 0},
    { id: 5, cartype: "mule", numberPlate: "Truck 6", locked: false, position: new mp.Vector3(913, -3129, 6), heading: 0},
    { id: 6, cartype: "mule", numberPlate: "Truck 7", locked: false, position: new mp.Vector3(917, -3129, 6), heading: 0},
    { id: 7, cartype: "mule", numberPlate: "Truck 8", locked: false, position: new mp.Vector3(921, -3129, 6), heading: 0},
    { id: 8, cartype: "mule", numberPlate: "Truck 9", locked: false, position: new mp.Vector3(925, -3129, 6), heading: 0},
    { id: 9, cartype: "mule", numberPlate: "Truck 10", locked: false, position: new mp.Vector3(929, -3129, 6), heading: 0},
    { id: 10, cartype: "mule", numberPlate: "Truck 11", locked: false, position: new mp.Vector3(933, -3129, 6), heading: 0},
    { id: 11, cartype: "mule", numberPlate: "Truck 12", locked: false, position: new mp.Vector3(937, -3129, 6), heading: 0},
    { id: 12, cartype: "mule", numberPlate: "Truck 13", locked: false, position: new mp.Vector3(941, -3129, 6), heading: 0},
    { id: 13, cartype: "mule", numberPlate: "Truck 14", locked: false, position: new mp.Vector3(945.5, -3129, 6), heading: 0},
    { id: 14, cartype: "mule", numberPlate: "Truck 15", locked: false, position: new mp.Vector3(949, -3129, 6), heading: 0},
    { id: 15, cartype: "mule", numberPlate: "Truck 16", locked: false, position: new mp.Vector3(953, -3129, 6), heading: 0},
    { id: 16, cartype: "mule", numberPlate: "Truck 17", locked: false, position: new mp.Vector3(957, -3129, 6), heading: 0},
    { id: 17, cartype: "mule", numberPlate: "Truck 18", locked: false, position: new mp.Vector3(961, -3129, 6), heading: 0},
    { id: 18, cartype: "mule", numberPlate: "Truck 19", locked: false, position: new mp.Vector3(965, -3129, 6), heading: 0},
    { id: 19, cartype: "mule", numberPlate: "Truck 20", locked: false, position: new mp.Vector3(969, -3129, 6), heading: 0},
    { id: 20, cartype: "benson", numberPlate: "Truck 21", locked: false, position: new mp.Vector3(893, -3155.5, 6), heading: 180},
    { id: 21, cartype: "benson", numberPlate: "Truck 22", locked: false, position: new mp.Vector3(897, -3155.5, 6), heading: 180},
    { id: 22, cartype: "benson", numberPlate: "Truck 23", locked: false, position: new mp.Vector3(901, -3155.5, 6), heading: 180},
    { id: 23, cartype: "benson", numberPlate: "Truck 24", locked: false, position: new mp.Vector3(905, -3155.5, 6), heading: 180},
    { id: 24, cartype: "benson", numberPlate: "Truck 25", locked: false, position: new mp.Vector3(909, -3155.5, 6), heading: 180},
    { id: 25, cartype: "benson", numberPlate: "Truck 26", locked: false, position: new mp.Vector3(913, -3155.5, 6), heading: 180},
    { id: 26, cartype: "benson", numberPlate: "Truck 27", locked: false, position: new mp.Vector3(917, -3155.5, 6), heading: 180},
    { id: 27, cartype: "benson", numberPlate: "Truck 28", locked: false, position: new mp.Vector3(921, -3155.5, 6), heading: 180},
    { id: 28, cartype: "benson", numberPlate: "Truck 29", locked: false, position: new mp.Vector3(925, -3155.5, 6), heading: 180},
    { id: 29, cartype: "benson", numberPlate: "Truck 30", locked: false, position: new mp.Vector3(929, -3155.5, 6), heading: 180},
    { id: 30, cartype: "benson", numberPlate: "Truck 31", locked: false, position: new mp.Vector3(933, -3155.5, 6), heading: 180},
    { id: 31, cartype: "benson", numberPlate: "Truck 32", locked: false, position: new mp.Vector3(937, -3155.5, 6), heading: 180},
    { id: 32, cartype: "benson", numberPlate: "Truck 33", locked: false, position: new mp.Vector3(941, -3155.5, 6), heading: 180},
    { id: 33, cartype: "benson", numberPlate: "Truck 34", locked: false, position: new mp.Vector3(945, -3155.5, 6), heading: 180},
    { id: 34, cartype: "benson", numberPlate: "Truck 35", locked: false, position: new mp.Vector3(949, -3155.5, 6), heading: 180},
    { id: 35, cartype: "benson", numberPlate: "Truck 36", locked: false, position: new mp.Vector3(953, -3155.5, 6), heading: 180},
    { id: 36, cartype: "benson", numberPlate: "Truck 37", locked: false, position: new mp.Vector3(957, -3155.5, 6), heading: 180},
    { id: 37, cartype: "benson", numberPlate: "Truck 38", locked: false, position: new mp.Vector3(961, -3155.5, 6), heading: 180},
    { id: 38, cartype: "benson", numberPlate: "Truck 39", locked: false, position: new mp.Vector3(965, -3155.5, 6), heading: 180},
    { id: 39, cartype: "benson", numberPlate: "Truck 40", locked: false, position: new mp.Vector3(969, -3155.5, 6), heading: 180}
]
car.map((car) => {
    const number1 = Math.floor(Math.random() * 256);
    const number2 = Math.floor(Math.random() * 256);
    let newcar = mp.vehicles.new(mp.joaat(car.cartype), car.position,
        {
            numberPlate: car.numberPlate,
            color: [[number1, number2, number1], [number1, number2, number1]],
            locked: false,
            heading: car.heading
        });
})


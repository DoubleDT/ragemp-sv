const UserService = require('../service/userService')
const ColShapeService = require('../service/enterColShapeService');

mp.events.add('event', (player) => {
    // if work with db, call services
    // e.g: UserService.createUser()
});

mp.events.add("OndutyTrucker", (player) => {
    ColShapeService.setTrucker(player);
});

mp.events.add("OffdutyTrucker", (player) => {
    ColShapeService.Offdutytrucker(player);
});

mp.events.add("UserInTruck", () => {
    const _player = mp.players.local;
    const blipAt = mp.blips.at(0);
    if (blipAt) return;
    const blip = mp.blips.new(1, new mp.Vector3(1244.721, -3297.95, 4.8), {
        name: "dia diem lay hang",
        color: 5
    });
    const colshapeLayhang = mp.colshapes.newSphere(1244.721, -3297.95, 4.8, 4);
    colshapeLayhang.setVariable("deleteBlipAndRout", 1);
    blip.routeFor(_player, 5, 2);
})

mp.events.add("playerEnterColshape", (player, colshape) => {
    const _player = mp.players.local;
    const value = colshape.getVariable('deleteBlipAndRout');
    if (value == 1) {
        let blip = mp.blips.at(0);
        if (blip) {
            blip.unrouteFor(player);
            blip.destroy();
            colshape.destroy();
            _player.vehicle.setVariable("TruckerTrunk", { loaihang: "", trongluong: 0 })
            return;
        }
    }
    else return
});

mp.events.add("UserGetMaterial", (player) => {
    const _player = mp.players.local;
    const blipAt = mp.blips.at(0);
    if (blipAt) return;
    const blip = mp.blips.new(1, new mp.Vector3(1244.737060546875, -3151.733642578125, 4), {
        name: "dia diem giao hang vat lieu",
        color: 5
    });
    _player.vehicle.setVariable("TruckerTrunk", { loaihang: "Vat lieu", trongluong: 200 });
    const colshapeLayhang = mp.colshapes.newSphere(1244.737060546875, -3151.733642578125, 4, 3);
    colshapeLayhang.setVariable("deleteBlipAndRout", 1);
    blip.routeFor(_player, 5, 2);
})

mp.events.add("UserGetAgriculture", () => {
    const _player = mp.players.local;
    const blipAt = mp.blips.at(0);
    if (blipAt) return;
    const blip = mp.blips.new(1, new mp.Vector3(1244.4449462890625, -3132.830322265625, 5), {
        name: "dia diem giao hang nong san",
        color: 5
    });
    _player.vehicle.setVariable("TruckerTrunk", { loaihang: "Nong san", trongluong: 200 });
    console.log(_player.vehicle.getVariable("TruckerTrunk"));
    const colshapeLayhang = mp.colshapes.newSphere(1244.737060546875, -3132.830322265625, 5, 3);
    colshapeLayhang.setVariable("deleteBlipAndRout", 1);
    blip.routeFor(_player, 5, 2);
})

mp.events.add("UserGetFood", (player) => {
    const _player = mp.players.local;
    const blipAt = mp.blips.at(0);
    if (blipAt) return;
    const blip = mp.blips.new(1, new mp.Vector3(1246.18212890625, -3142.593994140625, 5), {
        name: "dia diem giao hang thuc pham",
        color: 5
    });
    _player.vehicle.setVariable("TruckerTrunk", { loaihang: "Thuc pham", trongluong: 200 });
    const colshapeLayhang = mp.colshapes.newSphere(1244.737060546875, -3142.593994140625, 5, 3);
    colshapeLayhang.setVariable("deleteBlipAndRout", 1);
    blip.routeFor(_player, 5, 2);
})
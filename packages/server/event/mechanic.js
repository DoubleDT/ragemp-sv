const {colShapes} = require('../const/const')
const mechanicService = require('../service/mechanicService')

mp.events.add({
    'playerEnterColdShape-mechanicNPC': (player) => {
        mechanicService.bindKeyOpenMechanicNPC(player);
    },
    'keypress:Y:openMechanicNPC': (player)=>{
        mechanicService.openMechanicNPC(player)
    },
    'keypress:F4:openMechanicMenu': (player)=>{
        mechanicService.openMechanicMenu(player)
    },
    'setMechanicServer': (player)=>{
        mechanicService.setMechanic(player)
    },
    'repairVehServer': (player)=>{
        mechanicService.repairVeh(player)
    },
    'cleanVehServer': (player)=>{
        mechanicService.cleanVeh(player)
    },
})
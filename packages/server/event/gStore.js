const {colShapes} = require('../const/const')
const gStoreService = require('../service/gStoreService')

mp.events.add({
    'playerEnterColdShape-gStore': (player) => {
        gStoreService.bindKey(player);
    },
    'keypress:Y:openGStore': (player)=>{
        gStoreService.openGStore(player)
    },
    'purchaseItem': (player, item, price)=>{
        gStoreService.purchaseItem(player, item, price)
    },
})
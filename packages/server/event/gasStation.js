const {colShapes} = require('../const/const')
const gasStationSerVice = require('../service/gasStationService')

mp.events.add({
    'playerEnterColdShape-gasStation': (player,) => {
        gasStationSerVice.bindKey(player);
    },
    'keypress:Y:openGasStation': (player)=>{
        gasStationSerVice.openGasStation(player)
    },
    'fillUpServer': (player, car)=>{
        gasStationService.fillUp(player,car)
    },
})
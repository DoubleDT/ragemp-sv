    const userService = require('../service/userService');
    const {jobType} = require('../const/const')

    mp.events.add("loginServer", async (player, username, password) => {
        if(!player.data.token){
            let check = await userService.login({
                username,
                password
            });
            player.token = check.token;
            if (!check.error) { //login success
                player.money = 100000;
                player.ownCar = 0;
                player.car=[];
                return player.call("loginHandler", ["success"]);
            } else console.log(check.message);
        }
        else return player.call("loginHandler", ["logged"]);
        // else return console.log("incorrect");
        // } else {
        //     return player.call("loginHandler", ["incorrectInfo"])
        // }
    });

    mp.events.add("registerServer", async (player, username, password) => {
        if (username.length >= 3 && password.length >= 5) {
            let check = await userService.createUser({
                username,
                password
            });
            return player.call("loginHandler", ["registered"]);
        } else return player.call("loginHandler", ["tooshort"]);
    });

    mp.events.add("playerQuit", (player) => {
        if (player.data.username != "") {
            userService.saveInfo(player);
        }
    });

    mp.events.add("playerJoin", (player) => {
        console.log(`${player.name} has joined.`);
        player.job = jobType.unemploy
        player.data.username = "";
    });
const {colShapes} = require('../const/const')
const jobCenterService = require('../service/jobCenterService')

mp.events.add({
    'playerEnterColdShape-jobCenter': (player) => {
        jobCenterService.bindKey(player);
    },
    'keypress:Y:openJobCenter': (player)=>{
        jobCenterService.openJobCenter(player)
    },
    'setJobServer': (player, job)=>{
        jobCenterService.setJob(player, job)
    },
})
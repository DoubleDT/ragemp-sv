const ColShapeService = require('../service/enterColShapeService');
const {
    colShapes
} = require('../const/const');

// Tao marker
function generateColShapes() {
    let policeColShapes = mp.colshapes.newSphere(459, -991, 30, 1); //Tạo colshape để bắt sự kiện khi người chơi vào colshape
    policeColShapes.setVariable('SetC', colShapes.police); // Set biến và giá trị cho colshape
    mp.labels.new("Tủ đồ cảnh sát", new mp.Vector3(459.747, -991.5, 31), {
        drawDistance: 10
    }); //Tạo tên cho marker
    
    mp.markers.new(1, new mp.Vector3(459.747, -991.5, 29), 2, {
        color: [255, 255, 255, 100]
    }); //Tạo marker cho colshape

    //colshape chu cua hang pizza
    let pizzaBoyColshapes = mp.colshapes.newSphere(227.47271728515625, -21.79183578491211, 74.98737335205078, 1);
    pizzaBoyColshapes.setVariable('SetC', colShapes.pizzaBoy.npc);
    //marker xin viec + lay' banh'
    mp.labels.new("Chủ cừa hàng pizza", new mp.Vector3(227.47271728515625, -21.79183578491211, 75.5), {
        drawDistance: 10
    })

    mp.markers.new(0, new mp.Vector3(227.47271728515625, -21.79183578491211, 77), 2, {
        color: [255, 255, 255, 100]
    })
    
    //colshape lay xe giao pizza
    let pizzaVehicleColshapes = mp.colshapes.newSphere(221.1, -38.56, 69.57, 1);
    pizzaVehicleColshapes.setVariable('SetC', colShapes.pizzaBoy.spawnCar)
    //marker lay xe
    mp.labels.new("Lấy xe giao hàng", new mp.Vector3(221.1, -38.56, 70.5), {
        drawDistance: 10
    });
    mp.markers.new(1, new mp.Vector3(228, 222, 5), 2, {
        color: [255, 255, 255, 100]
    })





    // var truckerColShape = mp.colshapes.newSphere(797, -2982.11, 6, 1);
    // truckerColShape.setVariable('SetC', colShapes.trucker);
    // mp.labels.new("Chu nghe lai xe tai", new mp.Vector3(797.868, -2982.11, 7.4), { drawDistance: 10 });
    // mp.markers.new(0, new mp.Vector3(797.868, -2982.11, 7.4), 0.3, { color: [0, 255, 0, 100] });
}

mp.events.add("generatePizzaDes", player => {
    //colshape giao banh'
    let pizzaDeliverColshapes = mp.colshapes.newSphere(923.5, 3143.5, 38, 1);
    pizzaDeliverColshapes.setVariable('SetC', colShapes.pizzaBoy.deliverPizza)
    //marker giao pizza
    mp.labels.new("Giao pizza", new mp.Vector3(923.5, 3143.5, 39.5), {
        drawDistance: 10
    })
    mp.markers.new(1, new mp.Vector3(923.5, 3143.5, 38), 2, {
        color: [255, 255, 255, 100]
    })
})
generateColShapes();
//-----------------------------------//
//Event//
mp.events.add("playerEnterColshape", (player, colshape) => {
    var clPosition = colshape.getVariable('SetC'); //Set giá trị của colshape có tên là SetC vào clPosition
    switch (clPosition) {
        case colShapes.police: //nếu clPosition đúng thì set đồ và súng bên phần service k thì return
            return ColShapeService.giveWeapon(player);
        case colShapes.trucker:
            return ColShapeService.setTrucker(player);
        case colShapes.pizzaBoy.npc:
            return ColShapeService.openMenu(player);
        case colShapes.pizzaBoy.spawnCar:
            return ColShapeService.spawnPizzaVehicle(player);
        case colShapes.pizzaBoy.deliverPizza:
            return ColShapeService.deliverPizza(player);
        case colShapes.carShop:
            return ColShapeService.carShop(player);
        case colShapes.gasStation:
            return ColShapeService.gasStation(player);
        case colShapes.gStore:
            return ColShapeService.gStore(player);
        case colShapes.mechanic.npc:
            return ColShapeService.mechanic(player);
        case colShapes.jobCenter:
            return ColShapeService.jobCenter(player);
        case colShapes.wanted:
            return ColShapeService.wanted(player);
        default:
            return;
    }
});

mp.events.add('playerExitColshape', (player, shape) => {
    player.call("unbindYkey");
});

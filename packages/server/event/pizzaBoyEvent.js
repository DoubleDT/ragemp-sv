const pizzaBoyService = require('../service/pizzaBoyService')
const {jobType} = require('../const/const')

mp.events.add("onDuty", (player) => {
    return pizzaBoyService.onDuty(player);
})

mp.events.add("offDuty", (player) => {
    return pizzaBoyService.offDuty(player);
})

mp.events.add("quitJob", (player) =>{
    player.job = jobType.unemploy;
})

mp.events.add("takePizza",(player)=>{
    pizzaBoyService.getPizza(player);
})

mp.events.add("setPizzaboy",player => {
    player.job = jobType.shipper;
    player.outputChatBox("Ban da tro thanh Pizzaboy");
})

mp.events.add("keypress:Y", (player,toggle)=>{
    if(toggle) 
        return player.call('openNPCMenuPizza2')
    return player.call('openNPCMenuPizza')
})

mp.events.add("keypress:Y:Deliver", (player)=>{
    mp.vehicles.new(mp.joaat("bagger"), new mp.Vector3(221.1,-38.56,70.5),
    {
        numberPlate: "ADMIN",
        color: [[0, 255, 0],[0, 255, 0]]
    });
    player.outputChatBox("da spawn 1 xe giao pizza")
})
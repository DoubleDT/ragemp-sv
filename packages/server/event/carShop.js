const {colShapes} = require('../const/const')
const carShopService = require('../service/carShopService')

mp.events.add({
    'playerEnterColdShape-carShop': (player) => {
        carShopService.bindKey(player);
    },
    'keypress:Y:openCarShop': (player)=>{
        carShopService.openCarShop(player)
    },
    'purchaseCar': (player, car, carPrice)=>{
        carShopService.purchaseCar(player,car,carPrice)
    },
    'sellCar': (player,car) => {

    }
})
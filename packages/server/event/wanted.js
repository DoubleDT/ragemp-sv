const {colShapes} = require('../const/const')
const wantedService = require('../service/wantedService')

mp.events.add({
    'playerEnterColdShape-wanted': (player) => {
        wantedService.bindKey(player);
    },
    'keypress:Y:openWantedMenu': (player)=>{
        wantedService.openWantedMenu(player)
    },
    'emitWantedServer': (player, wantedID)=>{
        wantedService.emitWanted(player, wantedID)
    },
})
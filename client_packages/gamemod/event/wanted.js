const {wanted} = require('./gamemod/function/wantedFunction');

let wantedBrowser = null

mp.events.add({
    "openWantedMenuClient": ()=>{
        if(wantedBrowser == null){
            mp.gui.cursor.show(true,true);
            wantedBrowser = mp.browsers.new("package://assets/UI/wanted.html");
        }
        else mp.gui.chat.push("spam Y an kec ah");
    },
    "closeWantedMenu": ()=>{
        if(wantedBrowser)
        wantedBrowser.destroy();
        wantedBrowser = null;
        mp.gui.cursor.show(false,false);
    },
    "emitWantedClient": (wantedID)=>{
        wanted.emitWanted(wantedID)
    },
    "bindKeyToOpenWantedMenu": ()=>{
        mp.game.graphics.notify(`Nhan [Y] de mo menu truy na`);
        mp.keys.bind(0x59, true, () => {
            mp.gui.chat.push('Y key is pressed.');
            mp.events.callRemote('keypress:Y:openWantedMenu');
        })
    }
})
const {mechanic} = require('./gamemod/function/mechanicFunction');

let mechanicNPCMenu = null;
let mechanicMenu = null;

mp.events.add({
    "openMechanicNPCClient": ()=>{
        if (mechanicNPCMenu == null) {
            mp.gui.cursor.show(true,true);
            mechanicNPCMenu = mp.browsers.new("package://assets/UI/mechanicNPC.html")
        } else {
            mp.gui.chat.push("spam Y lam cho")
        }
    },
    "openMechanicMenuClient": ()=>{
        if (mechanicMenu == null) {
            mechanicMenu = mp.browsers.new("package://assets/UI/mechanicMenu.html")
            mp.gui.cursor.show(false,true);
        } else {
            mp.gui.chat.push("spam Y lam cho")
        }
    },
    "closeMechanicNPCMenu": ()=>{
        if(mechanicNPCMenu)
        mechanicNPCMenu.destroy();
        mechanicNPCMenu = null;
        mp.gui.cursor.show(false,false);
    },
    "closeMechanicMenu": ()=>{
        if(mechanicMenu)
        mechanicMenu.destroy();
        mechanicMenu = null;
        mp.gui.cursor.show(false,false);
    },
    "setMechanicClient": ()=>{
        mechanic.setMechanic();
    },
    "repairVehClient": ()=>{
        mechanic.repairVeh();
    },
    "cleanVehClient": ()=>{
        mechanic.cleanVeh();
    },
    "bindKeyToOpenMechanicNPC": ()=>{
        mp.game.graphics.notify(`Nhan [Y] de mo menu xin viec tho sua xe`);
        mp.keys.bind(0x59, true, () => {
            mp.gui.chat.push('Y key is pressed.');
            mp.events.callRemote('keypress:Y:openMechanicNPC');
        })
    },
    "bindKeyToOpenMechanicMenu": ()=>{
        mp.keys.bind(0x73, true, () => {
            mp.gui.chat.push('F4 key is pressed.');
            mp.events.callRemote('keypress:F4:openMechanicMenu');
        })
    },
})
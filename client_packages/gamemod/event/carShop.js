const { carShop } = require('./gamemod/function/carShopFunction')

let carBrowser = null

mp.events.add({
    "openCarShopClient": () => {
        if(carBrowser == null){
            mp.gui.cursor.show(true, true);
            carBrowser = mp.browsers.new("package://assets/UI/ccd.html");
        }
        else mp.gui.chat.push("spam Y an kec ah");
    },
    "closeCarShop": () => {
        if (carBrowser)
            carBrowser.destroy();
        carBrowser = null;
        mp.gui.cursor.show(false, false);
    },
    "purchaseCarClient": (car, carPrice) => {
        carShop.purchaseCar(car, carPrice);

    },
    "bindKeyToOpenCarShop": () => {
        mp.game.graphics.notify(`Nhan [Y] de mo cua hang xe`);
        mp.keys.bind(0x59, true, () => {
            mp.gui.chat.push('Y key is pressed.');
            mp.events.callRemote('keypress:Y:openCarShop');
        })
    }
})
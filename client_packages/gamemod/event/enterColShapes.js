const hud = require('./gamemod/Script/instructional_buttons/hudManager');
const { keyboard, menuHandle } = require('./gamemod/const');
const buttonHud = new hud(-1, "#171C20");
buttonHud.addButton("De hien thi menu nhan phim", 246)
let ColshapeTrucker = mp.colshapes.newSphere(797, -2982.11, 6, 1);
let ColshapeLayHang1 = mp.colshapes.newSphere(1244.721, -3297.95, 4.8, 2); // Toạ độ điểm lấy hàng 1
let ColshapeLayHang2 = mp.colshapes.newSphere(1244.2468, -3288.792, 4.7, 2); // Toạ độ điểm lấy hàng 2
const player = mp.players.local;

let browser = null;
function dongBrowser() {
    browser.destroy();
    browser = null;
    mp.gui.cursor.show(false, false);
}
mp.events.add("closeMenu", () => {
    dongBrowser();
})
//----Menu----//
function Menu() {
    mp.events.add("OndutyTrucker", () => {
        dongBrowser();
        mp.events.callRemote("OndutyTrucker");
        mp.game.ui.setNewWaypoint(923.5, -3143.5);
        mp.game.ui.notifications.showWithPicture("Trucker", "", "On duty", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
    })
    mp.events.add("OffdutyTrucker", () => {
        dongBrowser();
        mp.events.callRemote("OffdutyTrucker");
        mp.game.ui.notifications.showWithPicture("Trucker", "", "Off duty", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
    })
    mp.events.add("receiveTrucker", () => {
        dongBrowser();
        mp.game.ui.setNewWaypoint(923.5, -3143.5);
        mp.game.ui.notifications.showWithPicture("Trucker", "", "Ban da tro thanh tai xe^' lai xe tai", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
    })
    mp.events.add("revokeTrucker", () => {
        dongBrowser();
        mp.events.callRemote("OffdutyTrucker");
        mp.game.ui.notifications.showWithPicture("Trucker", "", "Ban da bo nghe trucker", "CHAR_BLOCKED", 1, false, 0, 139);
        let test = mp.colshapes.newSphere(797, -2982.11, 6, 1);
    })
}
function pressKey() {
    mp.events.add('playerExitColshape', (shape) => {
        mp.keys.unbind(keyboard.Y, true);
        buttonHud.toggleHud(false);
    });
    mp.keys.bind(keyboard.F2, true, function () {   // N Key
        const localPlayer = mp.players.local;
        let idVehicle = mp.game.vehicle.getClosestVehicle(localPlayer.position.x, localPlayer.position.y, localPlayer.position.z, 5, 0, 70);
        if (idVehicle != null) {
            const vehicle = mp.vehicles.atHandle(idVehicle);
            const test = vehicle.getVariable("TruckerTrunk");
            mp.gui.chat.push("Trong luong trong xe la: " + test.trongluong + "Kg");
            mp.gui.chat.push("Xe dang cho: " + test.loaihang);
        }

    });
}
function pressKeyY(menu) {
    mp.keys.bind(keyboard.Y, true, function () {
        if (menu == menuHandle.menuXinViec) {
            if (browser == null) {
                browser = mp.browsers.new("package://assets/UI/menu.html");
                mp.gui.cursor.show(true, true);
            }
            else {
                dongBrowser();
            }
        }
        else if (menu == menuHandle.menuLayHang) {
            if (browser == null) {
                browser = mp.browsers.new("package://assets/UI/menuLayHang.html");
                mp.gui.cursor.show(true, true);
            }
            else {
                dongBrowser();
            }
        }
    })
}
function enterColshape() {
    mp.events.add('playerEnterColshape', (shape) => {
        if (shape == ColshapeTrucker) {
            buttonHud.toggleHud(true);
            pressKeyY(menuHandle.menuXinViec);
        } else if (shape == ColshapeLayHang1 || shape == ColshapeLayHang2) {
            if (player.vehicle != null) {
                var vehicleID = player.vehicle.id;
                if (vehicleID <= 39) {
                    buttonHud.toggleHud(true);
                    pressKeyY(menuHandle.menuLayHang);
                }
            }
        }
        else {
            return;
        }
    });
}
function LayHang() {
    mp.events.add("LayNongSan", () => {
        dongBrowser();
        const _player = mp.players.local;
        mp.game.ui.notifications.showWithPicture("Trucker", "", "Dang lay Nong san, vui long cho 10s", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
        mp.gui.cursor.show(true, false);
        setTimeout(() => {
            mp.events.callRemote("UserGetAgriculture");
            mp.game.ui.notifications.showWithPicture("Trucker", "", "Da lay xong  Nong san, hay di giao hang", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
            mp.gui.cursor.show(false, false);
        }, 10000);
    });
    mp.events.add("LayNguyenLieu", () => {
        dongBrowser();
        const _player = mp.players.local;
        mp.game.ui.notifications.showWithPicture("Trucker", "", "Dang lay Nguyen Lieu, vui long cho 10s", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
        mp.gui.cursor.show(true, false);
        setTimeout(() => {
            mp.events.callRemote("UserGetMaterial");
            mp.game.ui.notifications.showWithPicture("Trucker", "", "da lay xong  Nguyen Lieu, hay di giao hang", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
            mp.gui.cursor.show(false, false);
        }, 10000);
    })
    mp.events.add("LayThucPham", () => {
        dongBrowser();
        const _player = mp.players.local;
        mp.game.ui.notifications.showWithPicture("Trucker", "", "Dang lay thuc pham, vui long cho 10s", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
        mp.gui.cursor.show(true, false);
        setTimeout(() => {
            mp.events.callRemote("UserGetFood");
            mp.game.ui.notifications.showWithPicture("Trucker", "", "Da lay xong thuc pham, hay di giao hang", "CHAR_LS_CUSTOMS", 1, false, 0, 139);
            mp.gui.cursor.show(false, false);
        }, 10000);
    })
}
function vehicle() {
    function playerEnterVehicleHandler(vehicle, seat) {
        const vehicleID = vehicle.id;
        if (vehicleID <= 39 && seat == 0) {
            mp.events.callRemote("UserInTruck");
        }
    }
    mp.events.add("playerEnterVehicle", playerEnterVehicleHandler);
    // mp.keys.bind(0x4E, true, function () {   // N Key
    //     if (mp.players.local.vehicle) {
    //         if (mp.players.local.vehicle.trunk) {
    //             mp.players.local.vehicle.trunk = false;
    //             mp.players.local.vehicle.setDoorShut(5, true);
    //         } else {
    //             mp.players.local.vehicle.trunk = true;
    //             mp.players.local.vehicle.setDoorOpen(5, false, true);
    //         }
    //     }
    // });

}


function total() {
    Menu();
    pressKey();
    enterColshape();
    LayHang();
    vehicle()
}
total();
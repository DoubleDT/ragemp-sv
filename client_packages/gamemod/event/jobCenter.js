const {jobType} = require('./gamemod/const')

let jobCenterBrowser = null;

mp.events.add({
    openJobCenterClient: () => {
        if(jobCenterBrowser == null){
            mp.gui.cursor.show(true, true);
            jobCenterBrowser = mp.browsers.new("package://assets/UI/jobcenter.html");
        }
        else mp.gui.chat.push("spam Y an kec ah");
    },

    selectJobClient: (option)=>{
        mp.events.callRemote("setJobServer", option);
    },

    "bindKeyToOpenJobCenter": () => {
        mp.game.graphics.notify(`Nhan [Y] de mo trung tam viec lam`);
        mp.keys.bind(0x59, true, () => {
            mp.gui.chat.push('Y key is pressed.');
            mp.events.callRemote('keypress:Y:openJobCenter');
        })
    },

    closeJobCenter: ()=>{
        if (jobCenterBrowser)
            jobCenterBrowser.destroy();
        jobCenterBrowser = null;
        mp.gui.cursor.show(false, false);
    }
});
const {gStore} = require('./gamemod/function/gStoreFunction');

let gStoreBrowser = null

mp.events.add({
    "openGStoreClient": ()=>{
        if(gStoreBrowser == null){
            mp.gui.cursor.show(true,true);
            gStoreBrowser = mp.browsers.new("package://assets/UI/gstore.html");
        }
        else mp.gui.chat.push("spam Y an kec ah");
    },
    "closeGStore": ()=>{
        if(gStoreBrowser)
        gStoreBrowser.destroy();
        gStoreBrowser = null;
        mp.gui.cursor.show(false,false);
    },
    "purchaseItemClient": (item, price)=>{
        gStore.purchaseItem(item, price);
    },
    "bindKeyToOpenGStore": ()=>{
        mp.game.graphics.notify(`Nhan [Y] de mo cua hang`);
        mp.keys.bind(0x59, true, () => {
            mp.gui.chat.push('Y key is pressed.');
            mp.events.callRemote('keypress:Y:openGStore');
        })
    }
})
let menuPizza = null;
//mo Menu khi player != pizzaboy gap NPC
mp.events.add("openNPCMenuPizza",()=>{
    if (menuPizza == null) {
        mp.gui.cursor.show(true,true);
        menuPizza = mp.browsers.new("package://assets/UI/menuPizza.html")
    } else {
        mp.gui.chat.push("spam lam con kiki")
    }
})

//mo Menu khi player == pizzaboy gap NPC
mp.events.add("openNPCMenuPizza2",()=>{
    if(menuPizza)
        menuPizza.destroy();
    menuPizza = mp.browsers.new("package://assets/UI/menuPizza.html")
    menuPizza.execute('$(".menu1").hide();')
    menuPizza.execute('$(".menu2").show();')
    mp.gui.cursor.show(true,true);
})

//dong browser
mp.events.add("closeBrowser",()=>{
    menuPizza.destroy();
    menuPizza = null;
    mp.gui.cursor.show(false, false);
})

mp.events.add("onDutyClient",(toggle)=>{
    mp.events.callRemote("onDuty", toggle);
})

mp.events.add("offDutyClient",(toggle)=>{
    mp.events.callRemote("offDuty", toggle);
})

mp.events.add("quitJobClient",()=>{
    mp.keys.unbind(0x73, true);
    mp.events.callRemote("quitJob");
})

mp.events.add("takePizzaClient",()=>{
    mp.events.callRemote("takePizza")
})

mp.events.add("setPizzaboyClient",()=>{
    mp.events.callRemote("setPizzaboy")
})

mp.events.add("enterPizzaColshapeClient",(toggle)=>{
    mp.game.graphics.notify(`Nhan [Y] de mo menu viec lam`);
    mp.keys.bind(0x59, true, () => {
        mp.gui.chat.push('Y key is pressed.');
        mp.events.callRemote('keypress:Y',toggle);
    
    })
})

mp.events.add("unbindYkey",()=>{
    mp.keys.unbind(0x59, true);
})

mp.events.add("deliverPizza", ()=>{
    mp.game.ui.setNewWaypoint(923.5,3143.5);
})

mp.events.add("bindKeyToSpawnVeh", ()=>{
    mp.game.graphics.notify(`Nhan [Y] de nhan xe giao pizza`);
    mp.keys.bind(0x59, true, () => {
        mp.gui.chat.push('Y key is pressed.');
        mp.events.callRemote('keypress:Y:Deliver');
    })
})


const {gasStation} = require('./gamemod/function/gasStationFunction')

let gasStationBrowser = null

mp.events.add({
    "openGasStationClient": ()=>{
        if(gasStationBrowser == null){
            mp.gui.cursor.show(true,true);
            gasStationBrowser = mp.browsers.new("package://assets/UI/gs.html");
        }
        else mp.gui.chat.push("spam Y an kec ah");
    },
    "closeGasStationMenu": ()=>{
        if(gasStationBrowser)
        gasStationBrowser.destroy();
        gasStationBrowser = null;
    mp.gui.cursor.show(false,false);
    },
    "fillUpClient": (car)=>{
        gasStation.fillUp(car);
        
    },
    "bindKeyToOpenGasStation": ()=>{
        mp.game.graphics.notify(`Nhan [Y] de do xang`);
        mp.keys.bind(0x59, true, () => {
        mp.gui.chat.push('Y key is pressed.');
        mp.events.callRemote('keypress:Y:openGasStation');
    })
    }
})
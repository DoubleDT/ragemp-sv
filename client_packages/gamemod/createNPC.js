const { carShopBlip, gasStationBlip, gStoreBlip, mechanicBlip, jobCenterBlip } = require('./gamemod/const')

let Ped = mp.peds.new(mp.game.joaat('u_m_y_pogo_01'), new mp.Vector3(797.868, -2982.11, 6), 95.0, (streamPed) => {
    streamPed.setAlpha(0);
});

let pizzaPed = mp.peds.new(mp.game.joaat('ig_chef2'), new mp.Vector3(227.47271728515625, -21.79183578491211, 74.98737335205078), 160, (streamPed) => {
    streamPed.setAlpha(0);
});

let carShopPed = carShopBlip.map(blip => {
    mp.peds.new(mp.game.joaat('cs_carbuyer'), new mp.Vector3(blip.x,blip.y,blip.z), 100, (streamPed) => {
        streamPed.setAlpha(0);
    });
})

let gasStationPed = gasStationBlip.map(blip => {
    mp.peds.new(mp.game.joaat('s_m_y_dwservice_01'), new mp.Vector3(blip.x,blip.y,blip.z), 100, (streamPed) => {
        streamPed.setAlpha(0);
    });
})

let gStorePed = gStoreBlip.map(blip => {
    mp.peds.new(mp.game.joaat('ig_josh'), new mp.Vector3(blip.x,blip.y,blip.z), 100, (streamPed) => {
        streamPed.setAlpha(0);
    });
})

let mechanicPed = mechanicBlip.map(blip => {
    mp.peds.new(mp.game.joaat('ig_josh'), new mp.Vector3(blip.x,blip.y,blip.z), 100, (streamPed) => {
        streamPed.setAlpha(0);
    });
})

let jobCenterPed = jobCenterBlip.map(blip => {
    mp.peds.new(mp.game.joaat('ig_andreas'), new mp.Vector3(blip.x,blip.y,blip.z), -100, (streamPed) => {
        streamPed.setAlpha(0);
    });
})
const doors = [
    { id: 1000, name: 'Police Station Main Enter Doors', hash: 320433149, locked: true, position: new mp.Vector3(434.7479, -983.2151, 30.83926) },  // Right
    { id: 1001, name: 'Police Station Main Enter Doors', hash: -1215222675, locked: true, position: new mp.Vector3(434.7479, -980.6184, 30.83926) },  // Left
    { id: 1002, name: 'Police Station Back Enter Doors', hash: -2023754432, locked: true, position: new mp.Vector3(469.9679, -1014.452, 26.53623) },  // Right
    { id: 1003, name: 'Police Station Back Enter Doors', hash: -2023754432, locked: true, position: new mp.Vector3(467.3716, -1014.452, 26.53623) },  // Left

    { id: 1004, name: 'Police Station Back To Cells Door', hash: -1033001619, locked: true, position: new mp.Vector3(463.4782, -1003.538, 25.00599) },
    { id: 1005, name: 'Police Station Cell Door 1', hash: 631614199, locked: true, position: new mp.Vector3(461.8065, -994.4086, 25.06443) },
    { id: 1006, name: 'Police Station Cell Door 2', hash: 631614199, locked: true, position: new mp.Vector3(461.8065, -997.6583, 25.06443) },
    { id: 1007, name: 'Police Station Cell Door 3', hash: 631614199, locked: true, position: new mp.Vector3(461.8065, -1001.302, 25.06443) },
    { id: 1008, name: 'Police Station Door To Cells Door', hash: 631614199, locked: true, position: new mp.Vector3(463.5701, -992.6641, 25.06443) },
    { id: 1009, name: 'Police Station Captans Office Door', hash: -1320876379, locked: true, position: new mp.Vector3(446.5728, -980.0106, 30.8393) },
    { id: 1010, name: 'Police Station Armory Double Door', hash: 185711165, locked: true, position: new mp.Vector3(450.1041, -984.0915, 30.8393) },  // Right
    { id: 1011, name: 'Police Station Armory Double Door', hash: 185711165, locked: true, position: new mp.Vector3(450.1041, -981.4915, 30.8393) },  // Left

    { id: 1012, name: 'Police Station Armory Secure Door', hash: 749848321, locked: true, position: new mp.Vector3(453.0793, -983.1895, 30.83926) },
    { id: 1013, name: 'Police Station Locker Rooms Door', hash: 1557126584, locked: true, position: new mp.Vector3(450.1041, -985.7384, 30.8393) },
    { id: 1014, name: 'Police Station Locker Room 1 Door', hash: -2023754432, locked: true, position: new mp.Vector3(452.6248, -987.3626, 30.8393) },
    { id: 1015, name: 'Police Station Roof Access Door', hash: 749848321, locked: true, position: new mp.Vector3(461.2865, -985.3206, 30.83926) },
    { id: 1016, name: 'Police Station Roof Door', hash: -340230128, locked: true, position: new mp.Vector3(464.3613, -984.678, 43.83443) },
    { id: 1017, name: 'Police Station Cell And Briefing Doors', hash: 185711165, locked: true, position: new mp.Vector3(443.4078, -989.4454, 30.8393) },  // Right
    { id: 1018, name: 'Police Station Cell And Briefing Doors', hash: 185711165, locked: true, position: new mp.Vector3(446.0079, -989.4454, 30.8393) },  // Left
    { id: 1019, name: 'Police Station Briefing Doors', hash: -131296141, locked: true, position: new mp.Vector3(443.0298, -991.941, 30.8393) },   // Right
    { id: 1020, name: 'Police Station Briefing Doors', hash: -131296141, locked: true, position: new mp.Vector3(443.0298, -994.5412, 30.8393) },  // Left
    { id: 1021, name: 'Police Station Back Gate Door', hash: -1603817716, locked: true, position: new mp.Vector3(489.301, -1020.029, 28.078) },
    { id: 1022, name: 'Police Station Living Door', hash: 749848321, locked: true, position: new mp.Vector3(447, -979.687, 30.6) },
    { id: 1023, name: 'Police Station Garage Door 1', hash: -822900180, locked: true, position: new mp.Vector3(459.44, -1019.727, 28) }
]
doors.map((door) => {
    mp.game.object.doorControl(door.hash, door.position.x, door.position.y, door.position.z, door.locked, 0.0, 0.0, 0);

    door.label = mp.labels.new(door.locked ? '~r~[Da khoa nhan E de mo]' : '~g~[ Mo nhan E de khoa ]', door.position,
        {
            los: false,
            font: 1,
            drawDistance: 3.5,
            color: [255, 255, 255, 255],
            dimension: 0
        });
});

mp.keys.bind(0x45, true, () => {
    doors.map((door) => {
        if (mp.game.gameplay.getDistanceBetweenCoords(door.position.x, door.position.y, door.position.z, mp.players.local.position.x, mp.players.local.position.y, mp.players.local.position.z, true) < 2.1) {
            door.locked = !door.locked;
            door.label.text = door.locked ? '~r~[ Da khoa nhan E de mo ]' : '~g~[ Mo nhan E de khoa ]';
            mp.game.object.doorControl(door.hash, door.position.x, door.position.y, door.position.z, door.locked, 0.0, 0.0, 0.0);
        }
    });
});
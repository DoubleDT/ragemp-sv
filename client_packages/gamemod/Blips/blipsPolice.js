
//Tao icon tren map
//Police
mp.blips.new(60, new mp.Vector3(446, -983, 29), {
    name: "LSPD/Don canh sat", // ten hien thi
    scale: 1, //ty le hien thi tren map
    color: 3, //mau icon
    alpha: 255, // độ đậm nhạt
    drawDistance: 100,
    shortRange: true,
    rotation: 0,
    dimension: 0,
});
//Trucker
mp.blips.new(318, new mp.Vector3(797.868, -2982.11, 7.4), {
    name: "Trucker", // ten hien thi
    scale: 1, //ty le hien thi tren map
    color: 83, //mau icon
    alpha: 255, // độ đậm nhạt
    drawDistance: 100,
    shortRange: true,
    rotation: 0,
    dimension: 0,
});
//Cho lay xe trucker
mp.blips.new(477, new mp.Vector3(923.5, -3143.5, 5.9), {
    name: "Xe truck", // ten hien thi
    scale: 1, //ty le hien thi tren map
    color: 81, //mau icon
    alpha: 255, // độ đậm nhạt
    drawDistance: 100,
    shortRange: true,
    rotation: 0,
    dimension: 0,
});
//Blips nhan hang
mp.blips.new(474, new mp.Vector3(1258,-3276.1,5.8), {
    name: "Dia diem lay hang", // ten hien thi
    scale: 1, //ty le hien thi tren map
    color: 81, //mau icon
    alpha: 255, // độ đậm nhạt
    drawDistance: 100,
    shortRange: true,
    rotation: 0,
    dimension: 0,
});

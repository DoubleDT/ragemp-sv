const {player_chat} = require('./gamemod/const');
const {PlayerFunction}  = require('./gamemod/function/playerFunction');
const { Register } = require('./gamemod/function/registerFunction')
const { Login } = require('./gamemod/function/loginFunction')
mp.events.add('playerChat', (text) => {
    switch (text) {
        case player_chat.greeting:
            return PlayerFunction.greeting()
        default:
            return PlayerFunction.default()
    }
})

mp.events.add('playerEnterCheckpoint', (checkpoint) => {
    switch(checkpoint) {
        case 1:
          //  return function
        case 2:
            //return function
        default:
            //return function
    }
})
// client (define event by yourself)
mp.gui.chat.show(true);
mp.gui.chat.activate(false);

var loginBrowser = mp.browsers.new("package://assets/UI/login.html");
mp.gui.cursor.show(true, true);

mp.events.add("registerClient", (user, pass) => {
    return Register.register({ user, pass });
});

mp.events.add("loginClient", (user, pass) => {
    return Login.login({ user, pass });
});

//need to test
mp.events.add("switchToRegister", () => {
    if (loginBrowser) {
        loginBrowser.destroy();
        loginBrowser = mp.browsers.new("package://assets/UI/register.html");
    }
});

mp.events.add("switchToLoginForm", () => {
    if(loginBrowser){
        loginBrowser.destroy();
    }
    loginBrowser = mp.browsers.new("package://assets/UI/login.html");
})

mp.events.add("loginHandler", (handle) => {
    return Login.handle(handle);
});
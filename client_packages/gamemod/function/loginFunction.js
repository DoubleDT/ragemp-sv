const {loginHandle} = require('./gamemod/const');

class Login {
    static async handle(handle) {
        switch (handle) {
            case loginHandle.success:
                {
                    loginBrowser.destroy();
                    mp.gui.chat.push("Login successful");
                    mp.gui.chat.activate(true);
                    mp.gui.cursor.show(false, false);
                    break;
                }
            case loginHandle.registered:
                {
                    loginBrowser.destroy();
                    mp.gui.chat.push("Registration successful");
                    mp.gui.chat.activate(true);
                    mp.gui.cursor.show(false, false);
                    break;
                }
            case loginHandle.incorrectUsername:
                {
                    loginBrowser.execute(`$(".incorrect-info").show(); $("#loginBtn").show();`);
                    break;
                }
            case loginHandle.tooshort:
                {
                    loginBrowser.execute(`$(".short-info").show(); $("#registerBtn").show();`);
                    break;
                }
            case loginHandle.logged:
                {
                    loginBrowser.execute(`$(".logged").show(); $("#loginBtn").show();`);
                    break;
                }
            default:
                {
                    break;
                }
            }
    };

    static async login( { user, pass } ){
        mp.events.callRemote("loginServer", user, pass);
    };
}

exports = { Login }
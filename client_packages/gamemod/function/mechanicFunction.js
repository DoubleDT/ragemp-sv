class mechanic{
    static async setMechanic(){
        mp.events.callRemote("setMechanicServer");
    };

    static async repairVeh(){
        mp.events.callRemote("repairVehServer");
    };

    static async cleanVeh(){
        mp.events.callRemote("cleanVehServer");
    }
}

exports = {mechanic};
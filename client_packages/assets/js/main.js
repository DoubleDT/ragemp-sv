function sendAccountInfo(state) {
    $('.alert-danger').hide();
    if (state === 0) { //Login State
        let loginName = document.getElementById("loginName");
        let loginPass = document.getElementById("loginPass");
        mp.trigger("loginClient", loginName.value, loginPass.value);
    } else {
        //Register State
        let registerName = document.getElementById("registerName");
        let registerPass = document.getElementById("registerPass");
        let registerPassCompare = document.getElementById("registerPass2");

        if (registerPass.value === registerPassCompare.value) {
            mp.trigger("registerClient", registerName.value, registerPass.value);
        } else {
            $(".password-mismatch").show();
            $("#registerBtn").show();
        }
    }
}

function switchToRegister() {
    mp.trigger("switchToRegister");
};

//pizza boy
function closeBrowser(){
    console.log('xyz')
    mp.trigger("closeBrowser");
}

function setPizzaboy(){
    mp.trigger("setPizzaboyClient");
}

function takePizza(){
    mp.trigger("takePizzaClient");
}

function onDuty(){
    mp.trigger("onDutyClient");
}

function offDuty(){
    mp.trigger("offDutyClient")
}

function quitJob(){
    mp.trigger("quitJobClient");
}